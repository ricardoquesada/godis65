// main

package main

import (
	"log"
	"os"

	"github.com/ricardoquesada/godis65"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatal("Invalid number of arguments. Filename expected")
	}

	p, err := godis65.NewPRG(os.Args[1])
	if err != nil {
		log.Fatal("Could not create project: ", err)
	}
	if err := p.Analyze(0xc000); err != nil {
		log.Fatal("Failed to trace: ", err)
	}
	p.DumpToConsole()
}
