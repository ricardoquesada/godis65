module github.com/ricardoquesada/mama

replace github.com/ricardoquesada/godis65 => ./godis65

go 1.12

require (
	github.com/gizak/termui/v3 v3.1.0 // indirect
	github.com/ricardoquesada/godis65 v0.0.0-00010101000000-000000000000
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
