
* = $c000

    lda #$00
    sta $d020
    ldx #$00

l0
    lda $c0,x
    sta $d021
    inx
    bne l0

    lda ($b8,x)
    sta ($b8),y

    lda $0000,b
    sty $1234
    stx $1234

    php
    clc
    bne l1

l1
    rts
    rti

    jmp $ff81
    jsr $ea81

    ldx #$00
l2
    lda data,x
    sta $d020
    inx
    cpx #$08
    bne l2
    rts

    lda scroll,x
    lda data,x
    lda some_addresses,x
    lda some_words,x
    rts

    .byte $00,$01,$02,$03,$04,$05,$06,$07
    .byte $00,$01,$02,$03,$04,$05,$06,$07
data
    .byte $00,$01,$02,$03,$04,$05,$06,$07
    .byte $00,$01,$02,$03,$04,$05,$06,$07

scroll
    .text "HELLO WORLD!", 0
    .text "hello world!", 0
    .text "0123456789",0

some_addresses
    .addr $0001,$ea81,$ff81,$fffe,$ea34
    .addr $1234,$eaea,$d020,$d021,$d022

some_words
    .word $1234,$1234,$0000,$0001,$0002
