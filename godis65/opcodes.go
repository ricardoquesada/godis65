// LICENSE

package godis65

type addressingMode int

const (
	Implicit        addressingMode = iota // NOP
	Accumulator                           // ROL A
	Immediate                             // LDA #$00
	ZeroPage                              // LDA $10
	ZeroPageX                             // LDY $10,X
	ZeroPageY                             // LDX $10,Y
	Relative                              // BNE *+02
	Absolute                              // LDA $1000
	AbsoluteX                             // LDA $1000,X
	AbsoluteY                             // LDA $1000,Y
	Indirect                              // JMP ($1000)
	IndexedIndirect                       // LDA ($10, X)
	IndirectIndexed                       // LDA ($10),Y
)

type Opcode struct {
	name         string
	mode         addressingMode
	len          int
	undocumented bool
}

var Opcodes = map[byte]Opcode{
	0x00: {"BRK", Implicit, 1, false},
	0x01: {"ORA", IndexedIndirect, 2, false},
	0x05: {"ORA", ZeroPage, 2, false},
	0x06: {"ASL", ZeroPage, 2, false},
	0x08: {"PHP", Implicit, 1, false},
	0x09: {"ORA", Immediate, 2, false},
	0x0A: {"ASL", Accumulator, 1, false},
	0x0D: {"ORA", Absolute, 3, false},
	0x0E: {"ASL", Absolute, 3, false},
	0x10: {"BPL", Relative, 2, false},

	0x11: {"ORA", IndirectIndexed, 2, false},
	0x15: {"ORA", ZeroPageX, 2, false},
	0x16: {"ASL", ZeroPageX, 2, false},
	0x18: {"CLC", Implicit, 1, false},
	0x19: {"ORA", AbsoluteY, 3, false},
	0x1D: {"ORA", AbsoluteX, 3, false},
	0x1E: {"ASL", AbsoluteX, 3, false},

	0x20: {"JSR", Absolute, 3, false},
	0x21: {"AND", IndexedIndirect, 2, false},
	0x24: {"BIT", ZeroPage, 2, false},
	0x25: {"AND", ZeroPage, 2, false},
	0x26: {"ROL", ZeroPage, 2, false},
	0x28: {"PLP", Implicit, 1, false},
	0x29: {"AND", Immediate, 2, false},
	0x2A: {"ROL", Accumulator, 1, false},
	0x2C: {"BIT", Absolute, 3, false},
	0x2D: {"AND", Absolute, 3, false},
	0x2E: {"ROL", Absolute, 3, false},

	0x30: {"BMI", Relative, 2, false},
	0x31: {"AND", IndirectIndexed, 2, false},
	0x35: {"AND", ZeroPageX, 2, false},
	0x36: {"ROL", ZeroPageX, 2, false},
	0x38: {"SEC", Implicit, 1, false},
	0x39: {"AND", AbsoluteY, 3, false},
	0x3D: {"AND", AbsoluteX, 3, false},
	0x3E: {"ROL", AbsoluteX, 3, false},

	0x40: {"RTI", Implicit, 1, false},
	0x41: {"EOR", IndexedIndirect, 2, false},
	0x45: {"EOR", ZeroPage, 2, false},
	0x46: {"LSR", ZeroPage, 2, false},
	0x48: {"PHA", Implicit, 1, false},
	0x49: {"EOR", Immediate, 2, false},
	0x4A: {"LSR", Accumulator, 1, false},
	0x4C: {"JMP", Absolute, 3, false},
	0x4D: {"EOR", Absolute, 3, false},
	0x4E: {"LSR", Absolute, 3, false},

	0x50: {"BVC", Relative, 2, false},
	0x51: {"EOR", IndirectIndexed, 2, false},
	0x55: {"EOR", ZeroPageX, 2, false},
	0x56: {"LSR", ZeroPageX, 2, false},
	0x58: {"CLI", Implicit, 1, false},
	0x59: {"EOR", AbsoluteY, 3, false},
	0x5D: {"EOR", AbsoluteX, 3, false},
	0x5E: {"LSR", AbsoluteX, 3, false},

	0x60: {"RTS", Implicit, 1, false},
	0x61: {"ADC", IndexedIndirect, 2, false},
	0x65: {"ADC", ZeroPage, 2, false},
	0x66: {"ROR", ZeroPage, 2, false},
	0x68: {"PLA", Implicit, 1, false},
	0x69: {"ADC", Immediate, 2, false},
	0x6A: {"ROR", Accumulator, 1, false},
	0x6C: {"JMP", Indirect, 3, false},
	0x6D: {"ADC", Absolute, 3, false},
	0x6E: {"ROR", Absolute, 3, false},

	0x70: {"BVS", Relative, 2, false},
	0x71: {"ADC", IndirectIndexed, 2, false},
	0x75: {"ADC", ZeroPageX, 2, false},
	0x76: {"ROR", ZeroPageX, 2, false},
	0x78: {"SEI", Implicit, 1, false},
	0x79: {"ADC", AbsoluteY, 3, false},
	0x7D: {"ADC", AbsoluteX, 3, false},
	0x7E: {"ROR", AbsoluteX, 3, false},

	0x81: {"STA", IndexedIndirect, 2, false},
	0x84: {"STY", ZeroPage, 2, false},
	0x85: {"STA", ZeroPage, 2, false},
	0x86: {"STX", ZeroPage, 2, false},
	0x88: {"DEY", Implicit, 1, false},
	0x8A: {"TXA", Implicit, 1, false},
	0x8C: {"STY", Absolute, 3, false},
	0x8D: {"STA", Absolute, 3, false},
	0x8E: {"STX", Absolute, 3, false},

	0x90: {"BCC", Relative, 2, false},
	0x91: {"STA", IndirectIndexed, 2, false},
	0x94: {"STY", ZeroPageX, 2, false},
	0x95: {"STA", ZeroPageX, 2, false},
	0x96: {"STX", ZeroPageY, 2, false},
	0x98: {"TYA", Implicit, 1, false},
	0x99: {"STA", AbsoluteY, 3, false},
	0x9A: {"TXS", Implicit, 1, false},
	0x9D: {"STA", AbsoluteX, 3, false},

	0xA0: {"LDY", Immediate, 2, false},
	0xA1: {"LDA", IndexedIndirect, 2, false},
	0xA2: {"LDX", Immediate, 2, false},
	0xA4: {"LDY", ZeroPage, 2, false},
	0xA5: {"LDA", ZeroPage, 2, false},
	0xA6: {"LDX", ZeroPage, 2, false},
	0xA8: {"TAY", Implicit, 1, false},
	0xA9: {"LDA", Immediate, 2, false},
	0xAA: {"TAX", Implicit, 1, false},
	0xAC: {"LDY", Absolute, 3, false},
	0xAD: {"LDA", Absolute, 3, false},
	0xAE: {"LDX", Absolute, 3, false},

	0xB0: {"BCS", Relative, 2, false},
	0xB1: {"LDA", IndirectIndexed, 2, false},
	0xB4: {"LDY", ZeroPageX, 2, false},
	0xB5: {"LDA", ZeroPageX, 2, false},
	0xB6: {"LDX", ZeroPageY, 2, false},
	0xB8: {"CLV", Implicit, 1, false},
	0xB9: {"LDA", AbsoluteY, 3, false},
	0xBA: {"TSX", Implicit, 1, false},
	0xBC: {"LDY", AbsoluteX, 3, false},
	0xBD: {"LDA", AbsoluteX, 3, false},
	0xBE: {"LDX", AbsoluteY, 3, false},

	0xC0: {"CPY", Immediate, 2, false},
	0xC1: {"CMP", IndexedIndirect, 2, false},
	0xC4: {"CPY", ZeroPage, 2, false},
	0xC5: {"CMP", ZeroPage, 2, false},
	0xC6: {"DEC", ZeroPage, 2, false},
	0xC8: {"INY", Implicit, 1, false},
	0xC9: {"CMP", Immediate, 2, false},
	0xCA: {"DEX", Implicit, 1, false},
	0xCC: {"CPY", Absolute, 3, false},
	0xCD: {"CMP", Absolute, 3, false},
	0xCE: {"DEC", Absolute, 3, false},

	0xD0: {"BNE", Relative, 2, false},
	0xD1: {"CMP", IndirectIndexed, 2, false},
	0xD5: {"CMP", ZeroPageX, 2, false},
	0xD6: {"DEC", ZeroPageX, 2, false},
	0xD8: {"CLD", Implicit, 1, false},
	0xD9: {"CMP", AbsoluteY, 3, false},
	0xDD: {"CMP", AbsoluteX, 3, false},
	0xDE: {"DEC", AbsoluteX, 3, false},

	0xE0: {"CPX", Immediate, 2, false},
	0xE1: {"SBC", IndexedIndirect, 2, false},
	0xE4: {"CPX", ZeroPage, 2, false},
	0xE5: {"SBC", ZeroPage, 2, false},
	0xE6: {"INC", ZeroPage, 2, false},
	0xE8: {"INX", Implicit, 1, false},
	0xE9: {"SBC", Immediate, 2, false},
	0xEA: {"NOP", Implicit, 1, false},
	0xEC: {"CPX", Absolute, 3, false},
	0xED: {"SBC", Absolute, 3, false},
	0xEE: {"INC", Absolute, 3, false},

	0xF0: {"BEQ", Relative, 2, false},
	0xF1: {"SBC", IndirectIndexed, 2, false},
	0xF5: {"SBC", ZeroPageX, 2, false},
	0xF6: {"INC", ZeroPageX, 2, false},
	0xF8: {"SED", Implicit, 1, false},
	0xF9: {"SBC", AbsoluteY, 3, false},
	0xFD: {"SBC", AbsoluteX, 3, false},
	0xFE: {"INC", AbsoluteX, 3, false},
}
