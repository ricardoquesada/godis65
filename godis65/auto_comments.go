// LICENSE

package godis65

var AutoComments = map[int]string{
	0xd000: "Sprite 0 X pos",
	0xd001: "Sprite 0 Y pos",
	0xd002: "Sprite 1 X pos",
	0xd003: "Sprite 1 Y pos",
	0xd004: "Sprite 2 X pos",
	0xd005: "Sprite 2 Y pos",
}
